package com.luxoft.bankapp.exception;

/**
 * Created by User on 13.02.14.
 */
public class BankException extends Exception{
    public BankException(String message){
        super(message);
    }
}
