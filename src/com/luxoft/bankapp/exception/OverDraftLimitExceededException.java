package com.luxoft.bankapp.exception;

import com.luxoft.bankapp.domain.bank.Account;

/**
 * Created by User on 13.02.14.
 */
public class OverDraftLimitExceededException extends NotEnoughFundsException{
    float maxSum;
    Account account;
    public OverDraftLimitExceededException(String mes, float ammount, Account account) {
        super(mes, ammount);
        maxSum = ammount;
        this.account = account;
    }

    public float getMaxSum(){
        return maxSum;
    }

    public Account getAccount(){
        return account;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(getClass().getName());
        sb.append(' ');
        sb.append(getMessage());
        sb.append(' ');
        sb.append("Max ammount ");
        sb.append(getMaxSum());

        return sb.toString();
    }
}
