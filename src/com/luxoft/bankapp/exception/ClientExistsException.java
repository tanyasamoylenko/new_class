package com.luxoft.bankapp.exception;

/**
 * Created by User on 13.02.14.
 */
public class ClientExistsException extends BankException{
    private String name;
    private String message;
    public ClientExistsException(String message, String name) {
        super(message);
        this.message = message;
        this.name = name;
    }

    public String getName(){
        return  name;
    }

    public void printMessage(){
        System.out.println(message);
    }
}
