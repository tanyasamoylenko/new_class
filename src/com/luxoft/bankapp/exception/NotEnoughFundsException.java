package com.luxoft.bankapp.exception;

/**
 * Created by User on 13.02.14.
 */
public class NotEnoughFundsException extends  BankException{
    private float amount;
    String message;

    public NotEnoughFundsException(String message, float amount){
        super(message);
        this.message = message;
        this.amount = amount;
    }

    public float getAmount(){
        return amount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(getClass().getName());
        sb.append(' ');
        sb.append(getMessage());
        sb.append(' ');
        sb.append("Amount ");
        sb.append(getAmount());

        return sb.toString();
    }

}
