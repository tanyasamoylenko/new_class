package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.OverDraftLimitExceededException;

/**
 * Created by Администратор on 13.02.14.
 */
public class CheckingAccount extends AbstractAccount {
    private float overdraft;

    public CheckingAccount(float overdraft) {
        this.overdraft = overdraft;
    }

    public void setOverdraft(float x){
        if(x>0)
            overdraft = x;
        else
            throw new IllegalArgumentException();
    }

    @Override
    public void printReport() {
        super.printReport();
        System.out.println("	Overdraft "+overdraft);
    }

    @Override
    public void withdraw(float x) throws OverDraftLimitExceededException{
        float sum = overdraft+super.getBalance();
        if(x>sum)
            throw new OverDraftLimitExceededException("Недостаточно денег на счету", sum, this);
        else if(x<=super.getBalance()){
            float newBal = super.getBalance()-x;
            super.setBalance(newBal);
        }else{
            super.setBalance(0);
            setOverdraft(sum-x);
        }

    }

    @Override
    public float maximumAmountToWithdraw(){
        float maxAmmount = getBalance()+overdraft;
        return maxAmmount;
    }

    @Override
    public boolean equals(Object object){
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;
        CheckingAccount account = (CheckingAccount) object;
        if (getIdAccount() != account.getIdAccount())
            return false;
        return true;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime*result+getIdAccount();
        return result;
    }
}

