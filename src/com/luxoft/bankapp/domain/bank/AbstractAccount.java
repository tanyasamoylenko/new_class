package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

/**
 * Created by Администратор on 13.02.14.
 */
public abstract class AbstractAccount implements Account {
    private static int idNumber = 0;
    private int idAccount;
    private float balance;

    public AbstractAccount() {
        balance = 0;
        idAccount = ++idNumber;
    }

    public void setBalance(float balance)throws IllegalArgumentException{
        if (balance < 0) {
            throw new IllegalArgumentException("Балланс меньше 0");
        }else
            this.balance = balance;
    }

    public float getBalance(){
        return balance;
    }

    public int getIdAccount(){
        return idAccount;
    }

    public void deposit(float x){
        balance+=x;
    }

    @Override
    public float maximumAmountToWithdraw() {
        return balance;
    }

    @Override
    public void printReport() {
        System.out.println("Balance "+balance);
    }
}
