package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

/**
 * Created by Администратор on 13.02.14.
 */
public class SavingAccount extends AbstractAccount {
    @Override
    public void withdraw(float x) throws NotEnoughFundsException {
        if(getBalance()<0)
            throw new NotEnoughFundsException("Недостаточно денег на счету", getBalance());
        else {
            float newBal = getBalance()-x;
            super.setBalance(newBal);
        }
    }

    @Override
    public float maximumAmountToWithdraw(){
        return super.maximumAmountToWithdraw();
    }

    @Override
    public void setBalance(float balance) {
        super.setBalance(balance);
    }

    @Override
    public float getBalance() {
        return super.getBalance();
    }

    @Override
    public boolean equals(Object object){
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;
        CheckingAccount account = (CheckingAccount) object;
        if (getIdAccount() != account.getIdAccount())
            return false;
        return true;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime*result+getIdAccount();
        return result;
    }
}
