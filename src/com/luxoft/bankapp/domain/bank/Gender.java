package com.luxoft.bankapp.domain.bank;

/**
 * Created by Администратор on 13.02.14.
 */
public enum Gender {
    MALE("Mr"), FEMALE("Ms");
    String solution;
    Gender(String solution){
        this.solution = solution;
    }

    public String getSalutation(){
        return solution;
    }
}
