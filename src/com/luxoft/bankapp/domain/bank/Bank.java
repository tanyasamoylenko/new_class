package com.luxoft.bankapp.domain.bank;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.service.bank.ClientRegistrationListener;

import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;

/**
 * Created by Администратор on 13.02.14.
 */
public class Bank implements Report {
    private String name;
    private ArrayList<Client>clients;
    private ArrayList<ClientRegistrationListener>listeners;
    private static final String DATA_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    public Bank(String name) {
        this.name = name;
        clients = new ArrayList<Client>();
        listeners = new ArrayList<ClientRegistrationListener>();

        class PrintClientListener implements ClientRegistrationListener{
            @Override
            public void onClientAdded(Client c) {
                c.printReport();
            }
        }

        class EmailNotificationListener implements ClientRegistrationListener{
            @Override
            public void onClientAdded(Client c) {
                System.out.println("Notification email for client "+c.getName()+" to be sent");
            }
        }

        class  DebugListener implements ClientRegistrationListener{
            @Override
            public void onClientAdded(Client c) {
                DateFormat dateFormat = new SimpleDateFormat(DATA_TIME_PATTERN);
                Date date = new Date();
                System.out.println(String.format("Client: %s Time: %s", c.getName(), dateFormat.format(date)));
            }
        }

        registerEvent(new PrintClientListener());
        registerEvent(new EmailNotificationListener());
        registerEvent(new DebugListener());
    }

    public void registerEvent(ClientRegistrationListener clientRegistrationListener){
        listeners.add(clientRegistrationListener);
    }

    public void addClient(Client client) throws ClientExistsException{
        boolean b = false;
        for(Client c: clients){
            if (client.equals(c))
                throw new ClientExistsException("Такой клиент уже существует", client.getName());
        }
        clients.add(client);
        for(ClientRegistrationListener c: listeners)
            c.onClientAdded(client);
    }

    public boolean removeClient(Client client){
        if(client != null){
            clients.remove(client);
            return true;
        }return false;
    }

    public ArrayList<Client> getClients(){
        return clients;
    }

    @Override
    public void printReport() {
        System.out.println("Банк: "+name+"\n");
        for(Client c: clients){
            c.printReport();
            System.out.print("Полный баланс: ");
            System.out.println(c.getBalance()+"\n");
        }
    }

    @Override
    public boolean equals(Object object){
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;
        Bank bank = (Bank) object;
        if (!name.equals(bank.name))
            return false;
        return true;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime*result+name.hashCode();
        return result;
    }

}
