package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.OverDraftLimitExceededException;
import com.luxoft.bankapp.service.bank.BankService;

/**
 * Created by Администратор on 13.02.14.
 */
public class BankApplication {
    private Bank bank;
    private BankService bankService;

    public BankApplication(String bankName){
        bank = new Bank(bankName);
        bankService = new BankService();
    }
    public void initialize(){
        Client client1 = new Client("Vasya", "Smirnov", Gender.MALE);
        CheckingAccount account1 = (CheckingAccount)client1.createAccount("c");
        client1.setActiveAccount(account1);
        client1.deposit(1000);
        SavingAccount account2 = (SavingAccount)client1.createAccount("s");
        client1.setActiveAccount(account2);
        client1.deposit(2000);

        Client client2 = new Client("Masha", "Vlasova", Gender.FEMALE);
        CheckingAccount account3 = (CheckingAccount)client2.createAccount("c");
        client2.setActiveAccount(account3);
        client2.deposit(1000);
        SavingAccount account4 = (SavingAccount)client2.createAccount("s");
        client2.setActiveAccount(account4);
        client2.deposit(2000);

        System.out.println();

        try {
            bankService.addClient(bank, client1);
            bankService.addClient(bank, client2);
            account1.setOverdraft(-1000);
            client2.withdraw(4000);
        }catch (ClientExistsException e){
            System.out.println(e.getMessage());
        }catch (NotEnoughFundsException e){
            System.out.println(e.getMessage()+". Максимальная сумма для снятия "+e.getAmount());
        }catch (IllegalArgumentException e){
            System.out.println("Попытка добавить отрицательный overdraft");
        }
    }

    public void printBalance(){
        bank.printReport();
    }

    public void test(){
        initialize();
        printBalance();
        modifyBank();
        printBalance();
    }

    public void modifyBank() {
        for(Client c : bank.getClients()){
            SavingAccount account = (SavingAccount)c.createAccount("s");
            c.setActiveAccount(account);
            c.deposit(1000);
            for(Account  a: c.getAccount()){
                try {
                    a.withdraw(50);
                }catch (NotEnoughFundsException e){
                    e.getMessage();
                }
            }
        }
    }

    public static void main(String[] args) {
        BankApplication ba = new BankApplication("Aval");
        ba.test();
    }
}
