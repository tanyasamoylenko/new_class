package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

/**
 * Created by Администратор on 13.02.14.
 */
public interface Account extends Report {
    float getBalance();
    void deposit(float x);
    void withdraw(float x) throws NotEnoughFundsException;
    float maximumAmountToWithdraw();
}
