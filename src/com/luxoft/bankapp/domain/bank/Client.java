package com.luxoft.bankapp.domain.bank;
import com.luxoft.bankapp.exception.NotEnoughFundsException;

import java.util.ArrayList;

/**
 * Created by Администратор on 13.02.14.
 */
public class Client implements Report {
    private String name;
    private String surname;
    private ArrayList<Account> accounts;
    private Account activeAccount;
    private float initialOverdraft;
    private Gender gender;

    public Client(String name, String surname, Gender gender) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        accounts = new ArrayList<Account>();
        initialOverdraft = 300;
    }

    public Client(String name, String surname, Gender gender,  float initialOverdraft) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.initialOverdraft =initialOverdraft;
        accounts = new ArrayList<Account>();
    }


    public void setActiveAccount(Account a) {
        activeAccount = a;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setInitialOverdraft(float x) {
        initialOverdraft = x;
    }

    public float getBalance() {
        float allBalance=0;
        for(Account a: accounts)
            allBalance+=a.getBalance();
        return allBalance;
    }

    public ArrayList<Account> getAccount(){
        return accounts;
    }

    public void deposit(float x){
        if(activeAccount != null)
            activeAccount.deposit(x);
    }

    public void withdraw(float x)throws NotEnoughFundsException{
        if(activeAccount != null)
            activeAccount.withdraw(x);
    }

    public String getName(){
        return name;
    }

    public String getSurname(){
        return surname;
    }

    public Account createAccount(String s){
        Account account;
        if(s.equals("c")){
            account = new CheckingAccount(initialOverdraft);
        }else{
            account = new SavingAccount();
        }
        accounts.add(account);
        return account;
    }

    @Override
    public void printReport() {
        System.out.println("Клиент: "+ name+" "+surname);
        int i=0;
        for(Account a: accounts){
            System.out.print("Account "+(++i)+": ");
            a.printReport();
        }
    }

    public void getClientSalutation(){
        System.out.println(gender.getSalutation() + " " + name);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;

        Client client = (Client) object;

        if (!name.equals(client.name)) return false;
        if (!surname.equals(client.surname)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime*result+name.hashCode();
        result = prime*result+surname.hashCode();
        result = prime*result+gender.hashCode();
        return result;
    }
}
