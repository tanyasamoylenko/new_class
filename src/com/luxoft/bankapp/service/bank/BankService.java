package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.ClientExistsException;

/**
 * Created by Администратор on 13.02.14.
 */
public class BankService {
    public void addClient(Bank bank, Client client)throws ClientExistsException{
        bank.addClient(client);
    }

    public void printMaximumAmountToWithdraw(Bank bank, Account account){
        System.out.println("Максимальная сумма для снятия: "+account.maximumAmountToWithdraw());
    }
}
